const express =require('express');
const server = express();
const {randomBytes} =require('crypto');
const bodyParser =require('body-parser')
const port =4001;

const commentsByPostId = { };
server.use(bodyParser.json());

server.get('/post/:id/comments',(req,res)=>{
    res.send(commentsByPostId[req.params.id]||[]);
});
server.post('/post/:id/comments',(req,res)=>{
    const commentId = randomBytes(4).toString('hex');
    const {content}= req.body;
    
    const comments = commentsByPostId[req.params.id]||[];
    comments.push({id:commentId,content});
    commentsByPostId[req.params.id]=comments;
    res.status(201).send(commentsByPostId);
});

server.listen(port,()=>{
    console.log(`app initialized on port ${port}`);
})